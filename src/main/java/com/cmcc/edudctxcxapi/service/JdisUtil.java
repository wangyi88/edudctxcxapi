package com.cmcc.edudctxcxapi.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

@Component
public class JdisUtil {
	
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private @Qualifier("redisSentinelPool0") JedisSentinelPool redisSentinelPoolNo1;
	
	public synchronized  Jedis getResource(){  
        Jedis jedis = null;  
        if(redisSentinelPoolNo1 != null){  
            try{  
                if(jedis == null ){  
                     jedis = redisSentinelPoolNo1.getResource();  
                }  
            }catch(Exception e){  
                logger.error(e.getMessage(),e);  
            }  
        }  
        return jedis;  
	}  
	  
	  /** 
	     * 回收Jedis对象资源 
	     * @param jedis 
	     */  
	    public synchronized  void returnResource(Jedis  jedis){  
	        if(jedis != null){  
	        	redisSentinelPoolNo1.returnResource(jedis);  
	        }  
	    }  
}
