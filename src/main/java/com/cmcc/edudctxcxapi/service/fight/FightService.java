package com.cmcc.edudctxcxapi.service.fight;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcc.edudctxcxapi.common.Constants;
import com.cmcc.edudctxcxapi.dao.fight.FightDao;
import com.cmcc.edudctxcxapi.model.UserInfo;
import com.cmcc.edudctxcxapi.service.JdisUtil;
import com.cmcc.edudctxcxapi.util.MyException;

import redis.clients.jedis.Jedis;

@Service
public class FightService {

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private FightDao fightDao;

	@Autowired
	private JdisUtil jdisUtil;

	public Object incrCard(String openId) {
		if (openId == null || "".equals(openId) || openId.isEmpty())
			return false;
		Jedis jedis = null;
		int flag = 0;
		String cardCount = null;
		try {
			jedis = jdisUtil.getResource();
			cardCount = jedis.get(Constants.DCT_JEDIS_STAR_KEY + openId);
			cardCount = cardCount == null ? "0" : cardCount;
			if (Integer.parseInt(cardCount) < 5) {
				UserInfo userInfo = fightDao.getShareNumberCueCard(openId);
				Integer cueCard = Integer.parseInt(userInfo.getCueCard()) + 1;
				Integer shareCount = Integer.parseInt(userInfo.getShareNumber()) + 1;
				userInfo.setCueCard(cueCard.toString());
				userInfo.setShareNumber(shareCount.toString());
				this.fightDao.saveAndFlush(userInfo);
				Integer cacheCount = Integer.parseInt(cardCount) + 1;
				jedis.setex(Constants.DCT_JEDIS_STAR_KEY + openId, cacheTime().intValue(), cacheCount.toString());
				return cueCard;
			} else if (Integer.parseInt(cardCount) == 5) {
				flag = 1;
				Integer cacheCount = Integer.parseInt(cardCount) + 1;
				jedis.setex(Constants.DCT_JEDIS_STAR_KEY + openId, cacheTime().intValue(), cacheCount.toString());
			} else {
				flag = 2;
			}
			
		} catch (Exception e) {
			logger.error("===========" + openId + "========incrCard============>" + e.getMessage());
		} finally {
			jdisUtil.returnResource(jedis);
			if (flag==1) {
				throw new MyException(-2, "分享次数已达上限");
			}else if(flag==2) {
				throw new MyException(-3, "分享次数已达上限,不提示");
			}
		}
		return 0;

	}

	/*
	 * 【根据OPENID 数量 减去用户提示卡】
	 * 
	 * @param openId
	 * 
	 * @return
	 * 
	 * @return
	 */
	public boolean minusCardByCount(String openId, Integer count) {
		logger.error("操作者的openId是=======" + openId + "减去提示卡的数量是===" + count);
		UserInfo user = fightDao.findByOpenId(openId);
		if (user != null) {
			int count1 = Integer.parseInt(user.getCueCard());
			if (count1 > 0 && count1 >= count) {
				count = count1 - count;
				if (count < 0) {
					count = 0;
				}
				user.setCueCard(count.toString());
				return this.fightDao.saveAndFlush(user) != null;
			}
		}
		return false;
	}

	public Object savePKRecord(String openId, Integer pkType, Integer isWin) {
		UserInfo user = this.fightDao.findByOpenId(openId);
		user.setPkAllCount(user.getPkAllCount() + 1);
		if (pkType == 1) {
			user.setAssaultCount(user.getAssaultCount() + 1);
			if (isWin == 1) {
				user.setPkAllWinCount(user.getPkAllWinCount() + 1);
				user.setAssaultWinCount(user.getAssaultWinCount() + 1);
			}
		} else {
			user.setDefenseCount(user.getDefenseCount() + 1);
			if (isWin == 1) {
				user.setPkAllWinCount(user.getPkAllWinCount() + 1);
				user.setDefenseWinCount(user.getDefenseWinCount() + 1);
			}
		}
		return this.fightDao.saveAndFlush(user);
	}

	public Long cacheTime() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Long seconds = (cal.getTimeInMillis() - System.currentTimeMillis()) / 1000;
		return seconds.longValue();
	}
}
