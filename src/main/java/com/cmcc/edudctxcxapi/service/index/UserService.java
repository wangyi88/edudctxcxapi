package com.cmcc.edudctxcxapi.service.index;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cmcc.edudctxcxapi.dao.fight.FightDao;
import com.cmcc.edudctxcxapi.dao.index.VirtualUserDao;
import com.cmcc.edudctxcxapi.model.UserInfo;
import com.cmcc.edudctxcxapi.model.VirtualUser;

import redis.clients.jedis.JedisSentinelPool;

@Service
public class UserService {

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private VirtualUserDao virtualUserDao;
	

	@Autowired
	private FightDao fightDao;
	
	@Autowired
	private @Qualifier("redisSentinelPool0") JedisSentinelPool redisSentinelPoolNo1;

	/**
	 * 根据openId查询用户信息
	 * 
	 * @param openId
	 * @return
	 */
	public UserInfo getUserById(String openId) {
		logger.error("获取到的openId是===============" + openId);
		String jpql = "select o from UserInfo o where o.openId=?1";
		List<Object> params = new ArrayList<Object>(1);
		params.add(openId);
		List<UserInfo> user = fightDao.findAllList(jpql, params);
		return user.size() > 0 ? user.get(0) : null;
	}

	/**
	 * 随机获取一条虚拟用户信息
	 * 
	 * @param
	 * @return
	 */
	public VirtualUser getRandomVirtualUser() {
		return virtualUserDao.getRandomVirtualUser();
	}

}
