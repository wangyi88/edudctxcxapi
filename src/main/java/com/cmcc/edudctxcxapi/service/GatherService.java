package com.cmcc.edudctxcxapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcc.edudctxcxapi.dao.fight.FightDao;
import com.cmcc.edudctxcxapi.dao.index.VirtualUserDao;
import com.cmcc.edudctxcxapi.dao.military.WordStoreDao;
import com.cmcc.edudctxcxapi.model.UserInfo;
import com.cmcc.edudctxcxapi.model.VirtualUser;
import com.cmcc.edudctxcxapi.model.WordStore;
import com.cmcc.edudctxcxapi.service.military.PowerMapService;

/**
 * 聚合接口
 * 
 * @author 廖俊锋 2018年7月26日
 */
@Service
public class GatherService {

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private PowerMapService powerMapService;

	@Autowired
	private FightDao fightDao;

	@Autowired
	private WordStoreDao wordStoreDao;

	@Autowired
	private VirtualUserDao virtualUserDao;
	
	private Random random = new Random();
	
	public Map<String, Object> getIndexData(String openId) {
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		List<UserInfo> userById = getUserById(openId);
		if (userById != null && userById.size() > 0) {
			hashMap.put("cardsNum", userById.get(0).getCueCard() == null ? 0 : userById.get(0).getCueCard());
		} else {
			hashMap.put("cardsNum", 0);
			createUser(openId);
		}
		List<Object[]> valueAll = powerMapService.getPowerMap(openId);
		hashMap.put("radarValue", valueAll);
		hashMap.put("openId", openId);
		return hashMap;
	}

	public List<UserInfo> getUserById(String openId) {
		String jpql = "select o from UserInfo o where o.openId=?1";
		List<Object> params = new ArrayList<Object>();
		params.add(openId);
		List<UserInfo> user = fightDao.findAllList(jpql, params);
		return user;
	}

	public void createUser(String openId) {
		try {
			UserInfo userInfo = new UserInfo(null, openId, null, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, "0", "0", null);
			fightDao.saveAndFlush(userInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Map<String, Object> randomWordPkData() {
		try {
			List<WordStore> randomWordStore = wordStoreDao.getRandomWordStore();
			VirtualUser findOne = virtualUserDao.findOne(random.nextInt(498)+1);
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("randomWordStore", randomWordStore);
			hashMap.put("randomVirtualUser", findOne);
			return hashMap;
		} catch (Exception e) {
			logger.error("-------randomWordPkData--------->" + e.getMessage());
		}
		return null;

	}
}
