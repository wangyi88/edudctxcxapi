package com.cmcc.edudctxcxapi.service.military;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcc.edudctxcxapi.dao.fight.FightDao;
import com.cmcc.edudctxcxapi.model.UserInfo;

/**
 * 用户雷达信息
 * 
 * @author youzhiqing
 * @date: 2018年6月30日 下午5:18:17
 */
@Service
public class PowerMapService {

	@Autowired
	private FightDao fightDao;

	public List<Object[]> getPowerMap(String openId) {
		Object[] str1 = { "道具", 30 };
		Object[] str2 = { "声望", 30 };
		Object[] str3 = { "胜率", 30 };
		Object[] str4 = { "防守", 30 };
		Object[] str5 = { "进攻", 30 };
		List<Object[]> powerList = Arrays.asList(str1, str2, str3, str4, str5);
		Map<String, Double> maxData = getMaxData();
		UserInfo user = this.fightDao.getShareNumberCueCard(openId);

		double getCueCard = Double.parseDouble(user.getCueCard());
		double getShareNumber = Double.parseDouble(user.getShareNumber());
		double getPkAllWinCount = user.getPkAllWinCount().doubleValue();
		double getPkAllCount = user.getPkAllCount().doubleValue();
		double getDefenseWinCount = user.getDefenseWinCount().doubleValue();
		double getDefenseCount = user.getDefenseCount().doubleValue();
		double getAssaultWinCount = user.getAssaultWinCount().doubleValue();
		double getAssaultCount = user.getAssaultCount().doubleValue();
		// 道具
		if (getCueCard > 0 && maxData.get("cueCards") != null && maxData.get("cueCards").doubleValue() > 0) {
			str1[1] = (getCueCard / maxData.get("cueCards").doubleValue()) * 70 + 30;
		}

		// 声望
		if (getShareNumber > 0 && maxData.get("currentNumbers") != null
				&& maxData.get("currentNumbers").doubleValue() > 0) {
			str2[1] = (getShareNumber / maxData.get("currentNumbers").doubleValue()) * 70 + 30;
		}

		// 胜利
		if (getPkAllWinCount > 0 && maxData.get("maxWinRate") != null && maxData.get("maxWinRate").doubleValue() > 0) {
			double maxWinRate = maxData.get("maxWinRate").doubleValue();
			str3[1] = getPkAllWinCount / getPkAllCount / maxWinRate * 70 + 30;
		}
		// 防守
		if (getDefenseWinCount > 0 && maxData.get("maxDefenseWinRate") != null
				&& maxData.get("maxDefenseWinRate").doubleValue() > 0) {
			double maxDefenseWinRate = maxData.get("maxDefenseWinRate").doubleValue();
			str4[1] = getDefenseWinCount / getDefenseCount / maxDefenseWinRate * 70 + 30;
		}

		// 进攻
		if (getAssaultWinCount > 0 && maxData.get("maxAssaultWinRate") != null
				&& maxData.get("maxAssaultWinRate").doubleValue() > 0) {
			double maxAssaultWinRate = maxData.get("maxAssaultWinRate").doubleValue();
			str5[1] = getAssaultWinCount / getAssaultCount / maxAssaultWinRate * 70 + 30;
		}

		return powerList;
	}

	private Map<String, Double> getMaxData() {
		Map<String, Double> maxData = new HashMap<String, Double>();
		Double currentNumbers = this.fightDao.currentNumbers();// 分享最多次数用户的分享次数
		Double cueCards = this.fightDao.cueCards();// 道具最多的用户
		maxData.put("maxWinRate", this.fightDao.maxWinRate());// 获取到胜率最高用户的胜率
		maxData.put("maxAssaultWinRate", this.fightDao.maxAssaultWinRate());// 获取到进攻胜率最高用户的防守胜率
		maxData.put("maxDefenseWinRate", this.fightDao.maxDefenseWinRate());// 防守胜率最高用户的防守胜率
		maxData.put("currentNumbers", currentNumbers == 0.0 ? 1.0 : currentNumbers);
		maxData.put("cueCards", cueCards == 0.0 ? 1.0 : cueCards);
		return maxData;
	}
}
