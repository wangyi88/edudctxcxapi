package com.cmcc.edudctxcxapi.service.military;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcc.edudctxcxapi.common.Constants;
import com.cmcc.edudctxcxapi.dao.military.WordStoreDao;
import com.cmcc.edudctxcxapi.model.Room;
import com.cmcc.edudctxcxapi.model.WordStore;
import com.cmcc.edudctxcxapi.service.JdisUtil;
import com.cmcc.edudctxcxapi.util.JsonUtils;
import com.cmcc.edudctxcxapi.util.ResultMsg;

import redis.clients.jedis.Jedis;

/**
 * PK房间业务逻辑层
 * 
 * @author 廖俊锋 2018年7月2日
 */
@Service
public class RoomService {

	@Autowired
	private WordStoreDao wordStoreDao;

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private JdisUtil jdisUtil;

	/**
	 * 通过IDS查题库
	 * 
	 * @param roomId
	 *            ->房间号
	 * @return
	 */
	public List<WordStore> getWordStoreByIds(String roomId) {
		Jedis jedis = null;
		try {
			jedis = jdisUtil.getResource();
			String string = jedis.get(Constants.ROOMREDISKEY + "" + roomId);
			if (string != null) {
				List<Object> params = new ArrayList<Object>();
				String jpql = "select o from WordStore o  where o.storeID in("
						+ JsonUtils.Json2Bean(string, Room.class).getPkStoreIds() + ") order BY store_id asc";
				return wordStoreDao.findAllList(jpql, params);
			}
			jdisUtil.returnResource(jedis);
			;
		} catch (Exception e) {
			logger.error("--RoomService--getWordStoreByIds--->" + e.getMessage());
		}
		return null;
	}

	public List<WordStore> randomWordStore() {
		return wordStoreDao.getRandomWordStore();
	}

	/**
	 * 获取随机10道题
	 * 
	 * @return
	 */
	private String randomWordStoreIds(List<WordStore> randomWordStore) {
		String ids = "";
		for (WordStore wordStore : randomWordStore) {
			ids += wordStore.getStoreID() + ",";
		}
		if (ids.length() > 2) {
			ids = ids.substring(0, ids.length() - 1);
		}
		randomWordStore = null;
		return ids;
	}

	public ResultMsg foundRoom(String openId, String roomId) {
		Jedis jedis = null;
		try {
			jedis = jdisUtil.getResource();
			List<WordStore> randomWordStore = wordStoreDao.getRandomWordStore();
			if (randomWordStore != null) {
				String randomWordStoreIds = randomWordStoreIds(randomWordStore);
				Room room = new Room(randomWordStoreIds, new Date().getTime(), openId, "0");
				jedis.setex(Constants.ROOMREDISKEY + roomId, 600, JsonUtils.Bean2Json(room));
				return ResultMsg.success("success", "WordStores", randomWordStore);
			}
		} catch (Exception e) {
			logger.error("-------foundRoom--------->"+e.getMessage());
		} finally {
			if (jedis != null) {
				jdisUtil.returnResource(jedis);
			}
		}
		return ResultMsg.desc(-1L, "创建失败");
	}

}
