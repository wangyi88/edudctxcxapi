package com.cmcc.edudctxcxapi.common;

public enum Constants {
	PKSESSIONID("PKSESSIONID", 1),	//记录WEBSCOKET SESSIONID和用户id关联
	ROOMREDISKEY("ROOMREDISKEY", 2),//记录房间
	DCT_JEDIS_STAR_KEY("JEDIS_STAR_KEY",3),//单词通小程序存放缓存的 用户分享次数/每天天时间数量的key的标识
	MAXUSERDATA("MAX_USER_DATA",4),//单词通小程序存放缓存的 用户分享次数/每天天时间数量的key的标识
	SCOKET_NET_STATE("SCOKET_NET_STATE",5);//单词通小程序存用户是否连接scoket
    // 成员变量  
    private String name;  
    
    private int index; 
    
    // 构造方法  
    private Constants(String name, int index) {  
        this.name = name;  
        this.index = index;  
    }  
}
