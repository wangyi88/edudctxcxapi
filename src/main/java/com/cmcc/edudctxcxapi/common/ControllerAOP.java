package com.cmcc.edudctxcxapi.common;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cmcc.edudctxcxapi.util.MyException;
import com.cmcc.edudctxcxapi.util.ResultBean;

/**
 * aop切面类
 * @author wangyi
 *
 */
public class ControllerAOP {
	
	private static final Logger logger = LoggerFactory.getLogger(ControllerAOP.class);
	
	public Object handlerControllerMethod(ProceedingJoinPoint pjp) {
		long startTime = System.currentTimeMillis();
		ResultBean<?> result;
		try {
			result = (ResultBean<?>) pjp.proceed();
			logger.info(pjp.getSignature() + "use time:"
					+ (System.currentTimeMillis() - startTime));
		} catch (Throwable e) {
			result = handlerException(pjp, e);
		}
		return result;
	}
	
	/**
	* 捕获异常
	*/
	private ResultBean<?> handlerException(ProceedingJoinPoint pjp, Throwable e) {
		@SuppressWarnings("rawtypes")
		ResultBean<?> result = new ResultBean();
		if(e instanceof MyException){
			result.setMsg(e.getMessage());
			result.setCode(((MyException) e).getErrorCode());
		}else {
			//如果是未知异常 可以通过发邮件的方式通知相关人员
			logger.error(pjp.getSignature() + " error ", e);
			result.setMsg(e.toString());
			result.setCode(ResultBean.FAIL);
		}
		return result;
	}
}
