package com.cmcc.edudctxcxapi.wxfunction.handler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import redis.clients.jedis.Jedis;

import com.cmcc.edudctxcxapi.common.Constants;
import com.cmcc.edudctxcxapi.model.Room;
import com.cmcc.edudctxcxapi.model.ScoketMsg;
import com.cmcc.edudctxcxapi.model.ScoketUserCache;
import com.cmcc.edudctxcxapi.service.JdisUtil;
import com.cmcc.edudctxcxapi.util.JsonUtils;

/**
 * PK页SCOKET监听
 * 
 * @author 廖俊锋 2018年6月29日
 */
public class PKWebSocketHandler implements org.springframework.web.socket.WebSocketHandler {

	private Logger logger = Logger.getLogger(getClass());

	// WebSocketSession不支持序列化 所以需要存储在内存
	public static Map<String, WebSocketSession> sessions = null;

	// 机器标识
	private final static String THE_MACHINE_ID = UUID.randomUUID().toString();

	//类装载的时候就创建的ConcurrentHashMap
	static {
		sessions = new ConcurrentHashMap<String, WebSocketSession>();
	}

	@Autowired
	private JdisUtil jdisUtil;

	/**
	 * 连接成功时候，会触的方法
	 */
	public void afterConnectionEstablished(WebSocketSession session) {

		HttpHeaders handshakeHeaders = session.getHandshakeHeaders();
		List<String> openId = handshakeHeaders.get("openId");
		Jedis jedis = null;
		try {
			jedis = jdisUtil.getResource();
			if (openId != null && openId.size() > 0) {// 有用户信息
				String key = openId.get(0);
				sessions.put(key, session);
				String user = jedis.get(key);
				jedis.set(THE_MACHINE_ID + session.getId(), key);// 机器标识+SCOKET会画Id存储到Redis 会话关闭时取用
				jedis.set(Constants.SCOKET_NET_STATE + key, key);// 这是存放所有连接webSocket服务的人到redis
				System.out.println(jedis.get(Constants.SCOKET_NET_STATE + key));
				
				
				// 如果用户请求头带有房间号 则获取该房间信息 并向双方推送对手信息
				List<String> roomIds = handshakeHeaders.get("roomId");
				if (roomIds != null && roomIds.size() > 0) {
					String roomId = roomIds.get(0);
					//获取房间号
					String cacheRoom = jedis.get(Constants.ROOMREDISKEY + roomId);
					if (cacheRoom != null && !cacheRoom.isEmpty()) {
						//获取房间信息
						Room room = JsonUtils.Json2Bean(cacheRoom, Room.class);
						//0是没人或者是只有一个人
						if ("0".equals(room.getState()) && room.getUserId1() != null
								&& !room.getUserId1().equals(key)) {// 房间有一个人 开始PK
							String user1 = jedis.get(room.getUserId1());
							System.out.println(user1);
							//组装信息 user1是加入人的信息
							ScoketMsg toThis = new ScoketMsg("101", user1, new Date().getTime(),
									JsonUtils.Json2Bean(user1, ScoketUserCache.class).getUserId(), key);
							//发送信息
							sendMessageToUserAndPush(key, transformString(JsonUtils.Bean2Json(toThis)));
							//组装信息
							ScoketMsg toSide = new ScoketMsg("101", user, new Date().getTime(), key,
									JsonUtils.Json2Bean(user1, ScoketUserCache.class).getUserId());
							//发送消息给对手
							sendMessageToUserAndPush(room.getUserId1(), transformString(JsonUtils.Bean2Json(toSide)));

							room.setUserId2(key);
							room.setState("1");
							jedis.setex(Constants.ROOMREDISKEY + roomId,
									jedis.ttl(Constants.ROOMREDISKEY + roomId).intValue(), JsonUtils.Bean2Json(room));

						} else if (room.getUserId1() == null) {// 没人
							room.setUserId1(key);
							jedis.setex(Constants.ROOMREDISKEY + roomId,
									jedis.ttl(Constants.ROOMREDISKEY + roomId).intValue(), JsonUtils.Bean2Json(room));

						} else if ("1".equals(room.getState())) {// 判断是否是断线重连
							if (key.equals(room.getUserId1())) {

								String user1 = jedis.get(room.getUserId1());
								ScoketMsg toThis = new ScoketMsg("102", user1, new Date().getTime(), null, key);
								sendMessageToUserAndPush(key, transformString(JsonUtils.Bean2Json(toThis)));
							} else if (key.equals(room.getUserId2())) {

								String user1 = jedis.get(room.getUserId2());
								ScoketMsg toThis = new ScoketMsg("102", user1, new Date().getTime(), null, key);
								sendMessageToUserAndPush(key, transformString(JsonUtils.Bean2Json(toThis)));
							}

						}

					}
				}
			} else {
				logger.error("openId乱七八糟的信息拿不到==========" + sessions.size() + "========");
			}
			if (jedis != null) {
				jdisUtil.returnResource(jedis);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

	}
	//
	public String transformString(String str) {
		try {
			return new String(str.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * js调用websocket.send时候，会调用该方法
	 */
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {

	}

	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {

	}

	/**
	 * 关闭连接时触发
	 */
	public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
		// 清除缓存中的记录
		Jedis jedis = jdisUtil.getResource();
		String userId = jedis.get(THE_MACHINE_ID + session.getId());
		jedis.del(THE_MACHINE_ID + session.getId());
		sessions.remove(userId);
		jedis.del(Constants.SCOKET_NET_STATE + userId);
		if (jedis != null) {
			jdisUtil.returnResource(jedis);
		}
		System.out.println("--关闭连接---》"+userId);
	}

	public boolean supportsPartialMessages() {
		return false;
	}

	/**
	 * 发送信息给指定用户
	 * 
	 * @param clientId
	 * @param message
	 * @return
	 */
	public static boolean sendMessageToUser(String userId, String message) {
		if (sessions.get(userId) == null) {
			return false;
		}
		WebSocketSession session = sessions.get(userId);
		if (!session.isOpen()) {
			return false;
		}
		try {
			session.sendMessage(new TextMessage(message));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 发送信息给指定用户
	 * 
	 * @param clientId
	 * @param message
	 * @return
	 */
	public boolean sendMessageToUserAndPush(String userId, String message) {
		//发送给某个人消息  如果这个人没有连接我这台机器的websocket就通过redis 发布给其他机器
		if (sessions.get(userId) == null) {
			Jedis resource = jdisUtil.getResource();
			//发布消息给频道 让其他机器的用户也可以收到消息
			resource.publish("DCTXCX", message);
			jdisUtil.returnResource(resource);
			System.out.println("--------------推送消息到其他服务器-------------------------->");
			return true;
		}
		WebSocketSession session = sessions.get(userId);
		if (!session.isOpen()) {
			logger.error("session 已经关闭=====");
			return false;
		}
		try {
			session.sendMessage(new TextMessage(message));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
