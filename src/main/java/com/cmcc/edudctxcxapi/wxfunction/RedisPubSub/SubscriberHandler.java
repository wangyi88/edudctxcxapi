package com.cmcc.edudctxcxapi.wxfunction.RedisPubSub;

import com.cmcc.edudctxcxapi.model.ScoketMsg;
import com.cmcc.edudctxcxapi.util.JsonUtils;
import com.cmcc.edudctxcxapi.wxfunction.handler.PKWebSocketHandler;

import redis.clients.jedis.JedisPubSub;

/**
 * 订阅消息监听    该类无法注入spring托管的类。。
 * 
 * @author 廖俊锋 2018年8月7日
 */
public class SubscriberHandler extends JedisPubSub {

	//从频道拿到订阅的消息 然后发送给对应的人 发布者收不到消息
	@Override
	public void onMessage(String channel, String message) {
		if ("DCTXCX".equals(channel)) {
			//收到的消息转化成实体类
			ScoketMsg msg = JsonUtils.Json2Bean(message, ScoketMsg.class);
			//获取发送者的userId
			String userId = msg.getSendUserId();
			//如果webSocket里面拿到对应的用户Id就发送消息给这个用户
			if (PKWebSocketHandler.sessions.get(userId) != null) {
				PKWebSocketHandler.sendMessageToUser(userId, message);
			}
		}
	}

	@Override
	public void onPMessage(String pattern, String channel, String message) {
		// TODO Auto-generated method stub

	}

	//这是订阅的时候
	@Override
	public void onSubscribe(String channel, int subscribedChannels) {
		// TODO Auto-generated method stub

	}

	//这是取消订阅的时候
	@Override
	public void onUnsubscribe(String channel, int subscribedChannels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPUnsubscribe(String pattern, int subscribedChannels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPSubscribe(String pattern, int subscribedChannels) {
		// TODO Auto-generated method stub

	}

}
