package com.cmcc.edudctxcxapi.wxfunction.RedisPubSub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import redis.clients.jedis.JedisSentinelPool;

/**
 * spring 加载完毕订阅频道
 * 这是spring加载完成以后就会立即执行的方法
 * 需要实现ApplicationListener重写onApplicationEvent方法
 * @author 廖俊锋 2018年8月7日
 */
@Service
public class Init implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private @Qualifier("redisSentinelPool0") JedisSentinelPool redisSentinelPoolNo1;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		//判断事件父节点是否为空 如果没有parent，他就是老大 如果不加这个判断就会调用两次
		if (event.getApplicationContext().getParent() == null) {// root application context
			//开启一个线程，因为订阅的时候会占用一个
			new Thread(new SubThread(redisSentinelPoolNo1)).start();
		}
	}

}
