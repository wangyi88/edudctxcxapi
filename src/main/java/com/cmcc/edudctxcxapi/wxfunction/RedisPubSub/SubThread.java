package com.cmcc.edudctxcxapi.wxfunction.RedisPubSub;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

/**
 * 因为订阅频道会使线程阻塞 启动一个线程完成初始化
 * 
 * @author 廖俊锋 2018年8月7日
 */
public class SubThread implements Runnable {

	//这是我们自己写的监听类
	private final SubscriberHandler subscriber = new SubscriberHandler();

	private JedisSentinelPool jedisPool;

	public SubThread(JedisSentinelPool jedisPool) {
		this.jedisPool = jedisPool;
	}

	@Override
	public void run() {
		Jedis resource = null;
		try {
			resource = jedisPool.getResource();
			//服务启动的时候订阅频道 多个服务都可以订阅这个频道
			resource.subscribe(subscriber, "DCTXCX");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (resource != null) {
				jedisPool.returnResource(resource);
			}

		}
	}
}
