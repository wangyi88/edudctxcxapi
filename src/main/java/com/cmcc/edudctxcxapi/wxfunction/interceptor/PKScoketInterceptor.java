package com.cmcc.edudctxcxapi.wxfunction.interceptor;


import java.util.Map;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

/**
 * webSocket默认配置  监听开始连接前和后的操作
 */
public class PKScoketInterceptor extends HttpSessionHandshakeInterceptor {

	 @Override  
	    public boolean beforeHandshake(ServerHttpRequest request,  
	            ServerHttpResponse response, WebSocketHandler wsHandler,  
	            Map<String, Object> attributes) throws Exception {  
	        System.out.println("Before Handshake");  
	        return super.beforeHandshake(request, response, wsHandler, attributes);  
	    }  
	  
	    @Override  
	    public void afterHandshake(ServerHttpRequest request,  
	            ServerHttpResponse response, WebSocketHandler wsHandler,  
	            Exception ex) {  
	        System.out.println("After Handshake");  
	        super.afterHandshake(request, response, wsHandler, ex);  
	    }  

}
