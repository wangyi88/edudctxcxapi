package com.cmcc.edudctxcxapi.wxfunction.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.HandshakeInterceptor;

import com.cmcc.edudctxcxapi.wxfunction.handler.PKWebSocketHandler;
import com.cmcc.edudctxcxapi.wxfunction.interceptor.PKScoketInterceptor;

/**
 * SCOKET 配置类
 * 
 * @author 廖俊锋 2018年6月29日
 */
@Configuration
@EnableWebMvc
@EnableWebSocket
public class PKWebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {

	//这个方法是配置ws路径 是方法后面的自定义路径  通过这个url可以连接到webSocket服务
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		//配置ws 访问
		PKScoketInterceptor scoketInterceptor = new PKScoketInterceptor();
		registry.addHandler(myWebSocketHandler(), new String[] { "/ws" })
				.addInterceptors(new HandshakeInterceptor[] { scoketInterceptor })
				.setAllowedOrigins(new String[] { "*" });

	}

	@Bean
	public PKWebSocketHandler myWebSocketHandler() {
		return new PKWebSocketHandler();
	}
}