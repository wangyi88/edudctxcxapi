package com.cmcc.edudctxcxapi.dao.fight;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cmcc.edudctxcxapi.common.BaseDao;
import com.cmcc.edudctxcxapi.model.UserInfo;


@Repository
public interface FightDao extends JpaRepository<UserInfo, Integer>, JpaSpecificationExecutor<UserInfo>, BaseDao<UserInfo, Integer>{

	@Query(value="SELECT * FROM EDU_DCT_XCX A WHERE A.OPEN_ID=?1",nativeQuery=true)
	public UserInfo findByOpenId(String openId);//获取所有微信用户基本信息
	
	@Query(value="SELECT * FROM EDU_DCT_XCX W WHERE W.OPEN_ID=?1",nativeQuery=true)
	public UserInfo getShareNumberCueCard(String openId);////获取分享次数/提示卡数
	
	@Query(value="SELECT MAX(TO_NUMBER(A.SHARE_NUMBER)) FROM  EDU_DCT_XCX A",nativeQuery=true)
	public Double currentNumbers();//分享最多次数用户的分享次数

	@Query(value="SELECT MAX(TO_NUMBER(A.CUECARD)) FROM EDU_DCT_XCX A",nativeQuery=true)
	public Double cueCards();//道具最多的用户
	
	@Query(value ="SELECT MAX(A.PK_ALL_WIN_COUNT/A.PK_ALL_COUNT) FROM EDU_DCT_XCX A  WHERE  A.PK_ALL_WIN_COUNT >0 and A.PK_ALL_COUNT>0" ,nativeQuery = true)
	Double maxWinRate();
	
	@Query(value ="SELECT MAX(A.ASSAULT_WIN_COUNT/A.ASSAULT_COUNT) FROM EDU_DCT_XCX A WHERE  A.ASSAULT_WIN_COUNT >0 and A.ASSAULT_COUNT>0" ,nativeQuery = true)
	Double maxAssaultWinRate();
	
	@Query(value ="SELECT MAX(A.DEFENSE_WIN_COUNT/A.DEFENSE_COUNT) FROM EDU_DCT_XCX A WHERE  A.DEFENSE_WIN_COUNT >0 and A.DEFENSE_COUNT>0" ,nativeQuery = true)
	Double maxDefenseWinRate();
	
}
