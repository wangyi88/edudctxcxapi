package com.cmcc.edudctxcxapi.dao.index;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cmcc.edudctxcxapi.common.BaseDao;
import com.cmcc.edudctxcxapi.model.VirtualUser;


public interface VirtualUserDao extends JpaRepository<VirtualUser, Integer>, JpaSpecificationExecutor<VirtualUser>,BaseDao<VirtualUser, Integer> {
	
	@Query(value = "SELECT * FROM (SELECT * FROM EDU_DCT_VIRTUAL_USER order by dbms_random.value) WHERE rownum =1", nativeQuery = true)
	public VirtualUser getRandomVirtualUser(); // 获取随机虚拟用户信息

}
