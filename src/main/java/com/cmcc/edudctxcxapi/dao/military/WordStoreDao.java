package com.cmcc.edudctxcxapi.dao.military;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cmcc.edudctxcxapi.common.BaseDao;
import com.cmcc.edudctxcxapi.model.WordStore;

@Repository
public interface WordStoreDao
		extends JpaRepository<WordStore, Long>, JpaSpecificationExecutor<WordStore>, BaseDao<WordStore, Long> {
	
	@Query(value = " SELECT  *   FROM   (  SELECT   *  FROM   EDU_DCT_WORD_STORE  ORDER BY   DBMS_RANDOM.VALUE(1,3))  WHERE   QUESTION_TYPE<3 and ROWNUM <= 10 ORDER BY store_id asc", nativeQuery = true)
	public List<WordStore> getRandomWordStore(); // 获取随机10道题

}
