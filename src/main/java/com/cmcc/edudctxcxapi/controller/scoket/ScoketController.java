package com.cmcc.edudctxcxapi.controller.scoket;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmcc.edudctxcxapi.common.Constants;
import com.cmcc.edudctxcxapi.model.Body;
import com.cmcc.edudctxcxapi.model.ScoketMsg;
import com.cmcc.edudctxcxapi.service.JdisUtil;
import com.cmcc.edudctxcxapi.util.JsonUtils;
import com.cmcc.edudctxcxapi.util.ResultMsg;
import com.cmcc.edudctxcxapi.wxfunction.handler.PKWebSocketHandler;

import redis.clients.jedis.Jedis;

/**
 * 
 * SCOKET发送消息 获取连接信息Controller
 * 
 * @author 廖俊锋 2018年6月29日
 */
@Controller
@RequestMapping("scoket")
public class ScoketController {

	@Autowired
	private PKWebSocketHandler handler;
	
	@Autowired
	private JdisUtil jdisUtil;
	
	/**
	 * 发送简单信息
	 * 
	 * @param request
	 * @param userId
	 *            ->用户id
	 * @param sendUserId
	 *            ->要发送的用户id
	 * @param msg
	 *            ->消息
	 * @return
	 */
	@RequestMapping("sendScoketMsg")
	@ResponseBody
	public Object sendScoketMsg(HttpServletRequest request, Body body) {
		String userId = request.getParameter("userId");
		String sendUserId = request.getParameter("sendUserId");
		String msgType = request.getParameter("msgType");
		if(queryUserState(sendUserId)) {
			ScoketMsg scoketMsg = new ScoketMsg(msgType, JsonUtils.Bean2Json(body), new Date().getTime(), userId,sendUserId);
			boolean flag = handler.sendMessageToUserAndPush(sendUserId, JsonUtils.Bean2Json(scoketMsg));
			if (flag) {
				return ResultMsg.success("OK");
			}
		}
		return ResultMsg.desc(-1L, "发送失败");
	}
	
	
	private boolean queryUserState(String openId) {
		Jedis resource = null;
		try {
			resource = jdisUtil.getResource();
			String string = resource.get(Constants.SCOKET_NET_STATE+openId);
			if(string!=null) {
				return true;
			}
		} finally {
			if(resource!=null) jdisUtil.returnResource(resource);
		}
		return false;
	}
	
}
