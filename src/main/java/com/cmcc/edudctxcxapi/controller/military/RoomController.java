package com.cmcc.edudctxcxapi.controller.military;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import redis.clients.jedis.Jedis;

import com.cmcc.edudctxcxapi.common.Constants;
import com.cmcc.edudctxcxapi.model.Room;
import com.cmcc.edudctxcxapi.model.ScoketUserCache;
import com.cmcc.edudctxcxapi.model.WordStore;
import com.cmcc.edudctxcxapi.service.JdisUtil;
import com.cmcc.edudctxcxapi.service.military.RoomService;
import com.cmcc.edudctxcxapi.util.JsonUtils;
import com.cmcc.edudctxcxapi.util.ResultMsg;

/**
 * PK房间
 * 
 * @author 廖俊锋 2018年7月2日
 */
@Controller
@RequestMapping("room")
public class RoomController {

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private RoomService roomService;

	@Autowired
	private JdisUtil jdisUtil;

	/**
	 * 创建房间
	 * 
	 * @param openId->用户唯一标识
	 * @param roomId->前端创建的房间号
	 * @return
	 */
	@RequestMapping("foundRoom")
	@ResponseBody
	public Object foundRoom(@RequestParam("openId") String openId, @RequestParam("roomId") String roomId) {
		return roomService.foundRoom(openId, roomId);
	}

	/**
	 * 查询房间状态
	 * 
	 * @param roomId
	 *            ->房间ID
	 * @return
	 */
	@RequestMapping("queryRoomState")
	@ResponseBody
	public Object queryRoomState(@RequestParam("roomId") String roomId, @RequestParam("openId") String openId,
			@RequestParam("headPortrait") String headPortrait, @RequestParam("nickName") String nickName) {
		Jedis resource = jdisUtil.getResource();
		try {
			if (nickName != null && headPortrait != null) {
				ScoketUserCache scoketUserCache = new ScoketUserCache(openId, headPortrait, nickName, null, null);
				resource.set(openId, JsonUtils.Bean2Json(scoketUserCache));
			}
			String roomStr = resource.get(Constants.ROOMREDISKEY + roomId);
			if (roomStr != null) {
				Room room = JsonUtils.Json2Bean(roomStr, Room.class);
				if ("1".equals(room.getState())) {
					return ResultMsg.desc(-1L, " 房间已满");
				}
				List<WordStore> WordStores = roomService.getWordStoreByIds(roomId);
				if (WordStores != null) {
					return ResultMsg.success("success", "WordStores", WordStores,"remaining", resource.ttl(Constants.ROOMREDISKEY + roomId));
				}
				return ResultMsg.desc(-3L, "未知错误");
			}
			System.err.println("房间号-----》"+roomId+"---roomStr---->"+roomStr);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			jdisUtil.returnResource(resource);
		}
		return ResultMsg.desc(-2L, " 房间已过期");
	}

	/**
	 * 房间作废接口
	 * 
	 * @param roomId
	 *            ->房间ID
	 * @return
	 */
	@RequestMapping("cancellationRoom")
	@ResponseBody
	public Object cancellationRoom(@RequestParam("roomId") String roomId) {
		Jedis resource = null;
		try {
			resource = jdisUtil.getResource();
			String key = Constants.ROOMREDISKEY + "" + roomId;
			String string = resource.get(key);
			if (string != null && !string.isEmpty()) {
				resource.del(key);
			}
			if (resource != null) {
				jdisUtil.returnResource(resource);
			}
			return ResultMsg.success(" 房间作废成功");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return ResultMsg.desc(-1L, " 房间作废失败");
	}

	/**
	 * 离开房间接口
	 * 
	 * @param roomId
	 *            ->房间ID
	 * @return
	 */
	@RequestMapping("outRoom")
	@ResponseBody
	public Object outRoom(@RequestParam("roomId") String roomId, @RequestParam("openId") String openId) {
		Jedis resource = null;
		try {
			resource = jdisUtil.getResource();
			String key = Constants.ROOMREDISKEY + roomId;
			String roomStr = resource.get(key);
			if (roomStr != null && !roomStr.isEmpty()) {
				Room room = JsonUtils.Json2Bean(roomStr, Room.class);
				if (openId.equals(room.getUserId1())) {
					room.setUserId1(null);
					resource.setex(key, resource.ttl(key).intValue(), JsonUtils.Bean2Json(room));
					logger.error("===========" + openId + "=======outRoom=========>" +roomId);
				}else if(openId.equals(room.getUserId2())){
					room.setUserId2(null);
					resource.setex(key, resource.ttl(key).intValue(), JsonUtils.Bean2Json(room));
					logger.error("===========" + openId + "=======outRoom=========>" +roomId);
				}
			}
			return ResultMsg.success("离开房间成功");
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			jdisUtil.returnResource(resource);
		}
		return ResultMsg.desc(-1L, " 离开房间失败");
	}

	/**
	 * 查询题库
	 * 
	 * @param roomId
	 *            ->房间ID
	 * @return
	 */
	@RequestMapping("getWordStoreByIds")
	@ResponseBody
	public Object getWordStoreByIds(@RequestParam("roomId") String roomId) {
		List<WordStore> WordStores = roomService.getWordStoreByIds(roomId);
		if (WordStores != null)
			return ResultMsg.success("获取题库成功", "WordStores", WordStores);
		return ResultMsg.desc(-1L, " 获取题库失败");
	}

}
