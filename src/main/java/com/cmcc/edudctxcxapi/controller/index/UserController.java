package com.cmcc.edudctxcxapi.controller.index;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.cmcc.edudctxcxapi.model.ScoketUserCache;
import com.cmcc.edudctxcxapi.model.UserInfo;
import com.cmcc.edudctxcxapi.model.VirtualUser;
import com.cmcc.edudctxcxapi.service.GatherService;
import com.cmcc.edudctxcxapi.service.JdisUtil;
import com.cmcc.edudctxcxapi.service.index.UserService;
import com.cmcc.edudctxcxapi.service.military.PowerMapService;
import com.cmcc.edudctxcxapi.util.AppPropertiesUtil;
import com.cmcc.edudctxcxapi.util.JsonUtils;
import com.cmcc.edudctxcxapi.util.ResultBean;
import com.cmcc.edudctxcxapi.util.ResultMsg;

import redis.clients.jedis.Jedis;

/**
 * 用户信息接口
 * 
 * @author 廖俊锋 2018年7月26日
 */
@Controller
@RequestMapping("/user")
public class UserController {

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private GatherService gatherService;

	@Autowired
	private JdisUtil jdisUtil;
	
	@Autowired
	private PowerMapService powerMapService;

	/**
	 * 查询用户信息,没有则保存一个
	 * 
	 * @param openId
	 * @return
	 */
	@RequestMapping("/getUser")
	@ResponseBody
	public ResultBean<UserInfo> getUserOrInsert(@RequestParam("openId") String openId) {
		return new ResultBean<UserInfo>(userService.getUserById(openId));
	}

	/**
	 * 保存用户信息进入缓存
	 * 
	 * @param userInfo
	 *            ->微信用户信息
	 * @return
	 */
	@RequestMapping("setCache")
	@ResponseBody
	public Object setCache(ScoketUserCache userInfo) {
		Jedis jedis = null;
		try {
			jedis = jdisUtil.getResource();
			if (userInfo.getUserId() != null) {
				jedis.set(userInfo.getUserId(), JsonUtils.Bean2Json(userInfo));
				return ResultMsg.success("缓存成功");
			}
		} catch (Exception e) {
			logger.error("----setCache---->" + e.getMessage());
		} finally {
			if (jedis != null) {
				jdisUtil.returnResource(jedis);
			}
		}
		return ResultMsg.desc(-1L, "缓存失败");
	}

	/**
	 * 调用微信接口返回openId
	 * 
	 * @param js_code
	 * @return
	 */
	@RequestMapping("getIndexData")
	@ResponseBody
	public ResultBean<Map<String, Object>> getIndexData(@RequestParam("code") String code) {
		logger.error("获取到的code是===============" + code);
		String url = AppPropertiesUtil.getValue("xcx_url");
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("appid", AppPropertiesUtil.getValue("xcx_appId"));
		map.add("secret", AppPropertiesUtil.getValue("xcx_secret"));
		map.add("grant_type", "authorization_code");
		map.add("js_code", code);
		long startTime = System.currentTimeMillis();
		logger.error("transfer wechat server start");
		String sessionKey = new RestTemplate().postForObject(url, map, String.class);
		logger.error("transfer wechat server end , use time============="+(System.currentTimeMillis()-startTime)+"s");
		Map<?, ?> json2Bean = JsonUtils.Json2Bean(sessionKey, HashMap.class);
		return new ResultBean<Map<String, Object>>(gatherService.getIndexData((String) json2Bean.get("openid")));
	}

	/**
	 * 随机获取一条虚拟用户信息
	 * 
	 * @return
	 */
	@RequestMapping("/getVirtualUser")
	@ResponseBody
	public ResultBean<VirtualUser> getVirtualUser() {
		return new ResultBean<VirtualUser>(userService.getRandomVirtualUser());
	}

	/**
	 * 查询题库
	 * 
	 * @param roomId
	 *            ->房间ID
	 * @return
	 */
	@RequestMapping("randomWordPkData")
	@ResponseBody
	public ResultBean<Map<String, Object>> randomWordPkData() {
		return new ResultBean<Map<String, Object>>(gatherService.randomWordPkData());
	}
	
	@RequestMapping("queryPowerMap")
	@ResponseBody
	public ResultBean<Object> queryPowerMap(@RequestParam("openId") String openId) {
		return new ResultBean<Object>(powerMapService.getPowerMap(openId));
	}
	
}
