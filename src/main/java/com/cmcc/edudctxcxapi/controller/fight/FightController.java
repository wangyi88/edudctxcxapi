package com.cmcc.edudctxcxapi.controller.fight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmcc.edudctxcxapi.service.fight.FightService;
import com.cmcc.edudctxcxapi.util.ResultBean;

/**
 * PK对战记录接口
 * 
 * @author 廖俊锋 2018年7月26日
 */
@Controller
@RequestMapping("/fightFriend")
public class FightController {

	@Autowired
	private FightService fightService;

	/**
	 * 分享获得一张提示卡
	 * 
	 * @param openId
	 *            用户唯一标识
	 * @return
	 */
	@RequestMapping("/shareNumberAndCuecard")
	@ResponseBody
	public ResultBean<Object> shareNumberAndCuecard(@RequestParam("openId") String openId) {
		return new ResultBean<Object>(fightService.incrCard(openId));
	}

	/**
	 * 减提示卡
	 * 
	 * @param openId
	 *            用户唯一标识
	 * @param count
	 *            数量
	 * @return
	 */
	@RequestMapping("/minusCardByCount")
	@ResponseBody
	public ResultBean<Boolean> minusCardByCount(@RequestParam("openId") String openId,
			@RequestParam("count") Integer count) {
		return new ResultBean<Boolean>(fightService.minusCardByCount(openId, count));
	}

	/**
	 * 
	 * 保存战绩记录
	 * 
	 * @param openId
	 *            用户唯一标识
	 * @param isWin
	 *            赢：1，其他：0
	 * @param pkType
	 *            1：进攻，0：防守
	 * @return
	 */
	@RequestMapping("/savePKRecord")
	@ResponseBody
	public ResultBean<Object> savePKRecord(@RequestParam("openId") String openId, @RequestParam("isWin") Integer isWin,
			@RequestParam("pkType") Integer pkType) {
		return new ResultBean<Object>(fightService.savePKRecord(openId, pkType, isWin));
	}

}
