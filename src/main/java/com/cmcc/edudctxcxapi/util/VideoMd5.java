package com.cmcc.edudctxcxapi.util;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author 作者：WangJian
 * @create 创建时间： 2018-05-28 上午 10:06
 **/
public class VideoMd5 {
    private static final String[] hexDigits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
    private static String byteArrayToHexString(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (byte b : bytes) {
            sb.append(byteToHexString(b));
        }
        return sb.toString();
    }

    private static String byteToHexString(byte b) {
        int n=b;
        if (n < 0) {
            n+=256;
        }
        int d1=n/16;
        int d2=n%16;
        return hexDigits[d1]+hexDigits[d2];
    }

    private static String MD5Encode(String origin, String charsetname) {
        String resultString=null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            if (charsetname == null || "".equals(charsetname)) {
                resultString = byteArrayToHexString(messageDigest.digest(origin.getBytes()));
            }else {
                resultString=byteArrayToHexString(messageDigest.digest(origin.getBytes(charsetname)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultString;
    }


    public static String getVideoUrl(String url){
        String[] urlArray = url.split("/");
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
        String time = format.format(Calendar.getInstance().getTime());
        String fileUrl="";
        for(int i=5;i<urlArray.length;i++) {
            fileUrl += urlArray[i]+"/";
        }
        fileUrl = fileUrl.substring(0, fileUrl.length() - 1);
        String toMD5EncodeString="andedu"+time+"/"+fileUrl;
        String md5EncodeString = MD5Encode(toMD5EncodeString, null);
        String videoUrl = urlArray[0]+"/"+urlArray[1]+"/"+urlArray[2]+"/"+time+"/"+md5EncodeString+"/"+fileUrl;
        return videoUrl;
    }


}
