package com.cmcc.edudctxcxapi.util;

import java.io.Serializable;
/**
 * 返回结果集实体类
 * @author wangyi
 */
public class ResultBean<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final int SUCCESS = 0;    //成功是0
	
	public static final int FAIL = -1;  	//失败是-1
	
	private String msg = "success";
	
	private int code = SUCCESS;
	
	private T data;

	public ResultBean() {
		super();
	}

	public ResultBean(T data) {
		super();
		this.data = data;
	}

	public ResultBean(Throwable e) {
		super();
		this.msg = e.toString();
		this.code = FAIL;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static int getSuccess() {
		return SUCCESS;
	}

	public static int getFail() {
		return FAIL;
	}
	
}
