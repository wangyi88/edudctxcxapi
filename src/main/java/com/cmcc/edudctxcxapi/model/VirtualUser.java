package com.cmcc.edudctxcxapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EDU_DCT_VIRTUAL_USER")
public class VirtualUser {
	
	@Id	
	@Column(name = "ID")
	private Integer id; 			//虚拟用户ID
	
	@Column(name = "VIRTUAL_NAME")
	private String virtualName;  	//虚拟用户名称
	
	@Column(name = "IMG_URL")
	private String  imgUrl;     	//图片

	public VirtualUser(Integer id, String virtualName, String imgUrl) {
		super();
		this.id = id;
		this.virtualName = virtualName;
		this.imgUrl = imgUrl;
	}

	public VirtualUser() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVirtualName() {
		return virtualName;
	}

	public void setVirtualName(String virtualName) {
		this.virtualName = virtualName;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
	
}
