package com.cmcc.edudctxcxapi.model;

import java.io.Serializable;

/**
 * 
 * 消息体
 * 
 * @author 廖俊锋 2018年6月29日
 */
public class Body implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4625128306336813403L;
	
	/*耗时*/
	private int  timeConsuming ;
	
	/*题号*/
	private int  questionNumber  ;
	
	/*此题得分*/
	private int  score ;
	
	/*总得分*/
	private int  scoreAll ;
	
	/*选择的选项*/
	private String selectOptions;
	
	private String roomId;
	
	public Body() {
		super();
	}
	
	public Body(int timeConsuming, int questionNumber, int score, int scoreAll, String selectOptions, String roomId) {
		super();
		this.timeConsuming = timeConsuming;
		this.questionNumber = questionNumber;
		this.score = score;
		this.scoreAll = scoreAll;
		this.selectOptions = selectOptions;
		this.roomId = roomId;
	}
	
	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getSelectOptions() {
		return selectOptions;
	}


	public void setSelectOptions(String selectOptions) {
		this.selectOptions = selectOptions;
	}


	public int getTimeConsuming() {
		return timeConsuming;
	}

	public void setTimeConsuming(int timeConsuming) {
		this.timeConsuming = timeConsuming;
	}

	public int getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber(int questionNumber) {
		this.questionNumber = questionNumber;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getScoreAll() {
		return scoreAll;
	}

	public void setScoreAll(int scoreAll) {
		this.scoreAll = scoreAll;
	}
	
	
	
}
