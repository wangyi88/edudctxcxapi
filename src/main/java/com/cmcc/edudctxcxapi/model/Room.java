package com.cmcc.edudctxcxapi.model;

import java.io.Serializable;

/**
 * PK房间实体类
 * 
 * @author 廖俊锋 2018年6月30日
 */
public class Room implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6812091268452117336L;

	/* 房间号 */
	private Long roomId;

	/* 题目 */
	private String pkStoreIds;

	/* 创建时间 */
	private Long createTime;

	/* 创建人 */
	private String userId1;

	/* 加入人 */
	private String userId2;
	
	/* 状态*/
	private String state;

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public String getPkStoreIds() {
		return pkStoreIds;
	}

	public void setPkStoreIds(String pkStoreIds) {
		this.pkStoreIds = pkStoreIds;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getUserId1() {
		return userId1;
	}

	public void setUserId1(String userId1) {
		this.userId1 = userId1;
	}

	public String getUserId2() {
		return userId2;
	}

	public void setUserId2(String userId2) {
		this.userId2 = userId2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Room(Long roomId, String pkStoreIds, Long createTime, String userId1, String userId2, String state) {
		super();
		this.roomId = roomId;
		this.pkStoreIds = pkStoreIds;
		this.createTime = createTime;
		this.userId1 = userId1;
		this.userId2 = userId2;
		this.state = state;
	}

	public Room(String pkStoreIds, Long createTime, String userId1, String state) {
		super();
		this.pkStoreIds = pkStoreIds;
		this.createTime = createTime;
		this.userId1 = userId1;
		this.state = state;
	}

	public Room() {
		super();
	}
	
	

}
