package com.cmcc.edudctxcxapi.model;

import java.io.Serializable;

/**
 * 用户连接到SCOKET时缓存用户个人数据
 * 
 * @author 廖俊锋 2018年6月29日
 */
public class ScoketUserCache implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8433804017094636074L;

	/* 微信用户唯一标识 */
	private String userId;

	/* 微信用户头像 */
	private String headPortrait;

	/* 微信用户昵称 */
	private String nickName;

	/* 微信用户所在城市 */
	private String city;

	/* 微信用户性别 */
	private String gender;
	
	public ScoketUserCache(String userId, String headPortrait, String nickName, String city, String gender) {
		super();
		this.userId = userId;
		this.headPortrait = headPortrait;
		this.nickName = nickName;
		this.city = city;
		this.gender = gender;
	}

	public ScoketUserCache() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHeadPortrait() {
		return headPortrait;
	}

	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "ScoketUserCache [userId=" + userId + ", headPortrait=" + headPortrait + ", nickName=" + nickName
				+ ", city=" + city + ", gender=" + gender + "]";
	}
}
