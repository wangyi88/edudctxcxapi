package com.cmcc.edudctxcxapi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/******
 * 微信用户信息表
 * @author pc
 *
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "EDU_DCT_XCX")
@SequenceGenerator(name="EDU_DCT_XCX_SEQUENCE",sequenceName="EDU_DCT_XCX_SEQUENCE",allocationSize=1)
public class UserInfo implements Serializable{
	 @Id
	 @Column(name = "ID")
	 @GeneratedValue(generator="EDU_DCT_XCX_SEQUENCE",strategy=GenerationType.SEQUENCE)
	private  Long id;
	 
	@Column(name = "OPEN_ID")   //小程序用户ID
	private  String openId;
	
    @Column(name = "USER_NICK_NAME")  // 小程序用户昵称
    private String userNickName;
    
    @Column(name = "ASSAULT_WIN_COUNT")   //进攻胜利次数
	private Double assaultWinCount;
    
    
    @Column(name = "DEFENSE_WIN_COUNT")   //防守胜利次数
    private Double defenseWinCount;
    
    
    @Column(name = "ASSAULT_COUNT")   //进攻次数
    private Double assaultCount;
    
    
    @Column(name = "DEFENSE_COUNT")   //防守次数
    private Double defenseCount;
    
    
    @Column(name = "PK_ALL_COUNT")   //PK次数
    private Double pkAllCount;
    
    @Column(name = "PK_ALL_WIN_COUNT")   //Pk胜利次数
    private Double pkAllWinCount;

    @Column(name = "CUECARD")   //提示卡
    private String cueCard;
    
    @Column(name = "SHARE_NUMBER")   //分享次数
    private String shareNumber;
    
    @Column(name = "REMARK")   //备注
    private String remark;

	public UserInfo(Long id, String openId, String userNickName, Double assaultWinCount, Double defenseWinCount,
			Double assaultCount, Double defenseCount, Double pkAllCount, Double pkAllWinCount, String cueCard,
			String shareNumber, String remark) {
		super();
		this.id = id;
		this.openId = openId;
		this.userNickName = userNickName;
		this.assaultWinCount = assaultWinCount;
		this.defenseWinCount = defenseWinCount;
		this.assaultCount = assaultCount;
		this.defenseCount = defenseCount;
		this.pkAllCount = pkAllCount;
		this.pkAllWinCount = pkAllWinCount;
		this.cueCard = cueCard;
		this.shareNumber = shareNumber;
		this.remark = remark;
	}

	public UserInfo() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUserNickName() {
		return userNickName;
	}

	public void setUserNickName(String userNickName) {
		this.userNickName = userNickName;
	}

	public Double getAssaultWinCount() {
		return assaultWinCount;
	}

	public void setAssaultWinCount(Double assaultWinCount) {
		this.assaultWinCount = assaultWinCount;
	}

	public Double getDefenseWinCount() {
		return defenseWinCount;
	}

	public void setDefenseWinCount(Double defenseWinCount) {
		this.defenseWinCount = defenseWinCount;
	}

	public Double getAssaultCount() {
		return assaultCount;
	}

	public void setAssaultCount(Double assaultCount) {
		this.assaultCount = assaultCount;
	}

	public Double getDefenseCount() {
		return defenseCount;
	}

	public void setDefenseCount(Double defenseCount) {
		this.defenseCount = defenseCount;
	}

	public Double getPkAllCount() {
		return pkAllCount;
	}

	public void setPkAllCount(Double pkAllCount) {
		this.pkAllCount = pkAllCount;
	}

	public Double getPkAllWinCount() {
		return pkAllWinCount;
	}

	public void setPkAllWinCount(Double pkAllWinCount) {
		this.pkAllWinCount = pkAllWinCount;
	}

	public String getCueCard() {
		return cueCard;
	}

	public void setCueCard(String cueCard) {
		this.cueCard = cueCard;
	}

	public String getShareNumber() {
		return shareNumber;
	}

	public void setShareNumber(String shareNumber) {
		this.shareNumber = shareNumber;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
