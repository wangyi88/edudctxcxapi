package com.cmcc.edudctxcxapi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="edu_dct_word_store")
@SequenceGenerator(name="EDU_DCT_WORD_STORE_ID",sequenceName="EDU_DCT_WORD_STORE_ID",allocationSize=300)
public class WordStore implements Serializable{
	

	@Id
	@Column(name="STORE_ID")
	@GeneratedValue(generator="EDU_DCT_WORD_STORE_ID",strategy=GenerationType.SEQUENCE)
	private Long storeID;     			//题库ID
	
	@Column(name="WORD_ID")
	private Long wordID;	  			//单词ID（逻辑外键）
	
	@Column(name="QUESTION_TYPE")
	private Long questionType;			//标识 0:题型1(根据单词选正确释义) 1:题型2(根据释义选正确单词) 2:题型3(单词填空,题目出处为真题例句)
	
	@Column(name="QUESTION_TITLE")
	private String questionTitle;		//题目
	
	@Column(name="OPTION_A")
	private String optionA;				//选项A
	
	@Column(name="OPTION_B")
	private String optionB;				//选项B

	@Column(name="OPTION_C")
	private String optionC;				//选项C
	
	@Column(name="OPTION_D")
	private String optionD;				//选项D
	
	@Column(name="TRUE_OPTION")
	private String trueOption;			//正确选项
	
	@Column(name="QUESTION_RESOURSE")
	private String questionResourse;	//真题出处	
	
	@Column(name="AUDIO_ADDRESS")
	private String audioAddress;		//读音文件地址
	
	@Column(name="WORD")
	private String word;		//读音文件地址

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Long getStoreID() {
		return storeID;
	}

	public void setStoreID(Long storeID) {
		this.storeID = storeID;
	}

	public Long getWordID() {
		return wordID;
	}

	public void setWordID(Long wordID) {
		this.wordID = wordID;
	}

	public Long getQuestionType() {
		return questionType;
	}

	public void setQuestionType(Long questionType) {
		this.questionType = questionType;
	}

	public String getQuestionTitle() {
		return questionTitle;
	}

	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}

	public String getOptionA() {
		return optionA;
	}

	public void setOptionA(String optionA) {
		this.optionA = optionA;
	}

	public String getOptionB() {
		return optionB;
	}

	public void setOptionB(String optionB) {
		this.optionB = optionB;
	}

	public String getOptionC() {
		return optionC;
	}

	public void setOptionC(String optionC) {
		this.optionC = optionC;
	}

	public String getOptionD() {
		return optionD;
	}

	public void setOptionD(String optionD) {
		this.optionD = optionD;
	}

	public String getTrueOption() {
		return trueOption;
	}

	public void setTrueOption(String trueOption) {
		this.trueOption = trueOption;
	}

	public String getQuestionResourse() {
		return questionResourse;
	}

	public void setQuestionResourse(String questionResourse) {
		this.questionResourse = questionResourse;
	}

	public String getAudioAddress() {
		return audioAddress;
	}

	public void setAudioAddress(String audioAddress) {
		this.audioAddress = audioAddress;
	}

	public WordStore() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WordStore(Long storeID, Long wordID, Long questionType, String questionTitle, String optionA, String optionB,
			String optionC, String optionD, String trueOption, String questionResourse, String audioAddress) {
		super();
		this.storeID = storeID;
		this.wordID = wordID;
		this.questionType = questionType;
		this.questionTitle = questionTitle;
		this.optionA = optionA;
		this.optionB = optionB;
		this.optionC = optionC;
		this.optionD = optionD;
		this.trueOption = trueOption;
		this.questionResourse = questionResourse;
		this.audioAddress = audioAddress;
	}
	
	
}
