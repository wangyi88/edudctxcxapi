package com.cmcc.edudctxcxapi.model;

import java.io.Serializable;

/**
 * 
 * SCOKET与前端进行数据交互的格式
 * 
 * @author 廖俊锋 2018年6月29日
 */
public class ScoketMsg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6829352346632603962L;

	/*
	 * 消息类型 1;提示信息 2：对手状态信息 3：对手答题分数信息 4：邀请信息 5加入信息
	 */
	private String msgType;

	/* 消息内容主题 */
	private String body;

	/* 时间戳 */
	private long timestamp;
	
	/* 发送人 */
	private String userId;
	
	/*接收人 */
	private String sendUserId;

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	public String getSendUserId() {
		return sendUserId;
	}

	public void setSendUserId(String sendUserId) {
		this.sendUserId = sendUserId;
	}

	public ScoketMsg(String msgType, String body, long timestamp, String userId,String sendUserId) {
		super();
		this.msgType = msgType;
		this.body = body;
		this.timestamp = timestamp;
		this.userId = userId;
		this.sendUserId =sendUserId;
	}

	public ScoketMsg() {
		super();
	}

	@Override
	public String toString() {
		return "ScoketMsg [msgType=" + msgType + ", body=" + body + ", timestamp=" + timestamp + "]";
	}
	
}
